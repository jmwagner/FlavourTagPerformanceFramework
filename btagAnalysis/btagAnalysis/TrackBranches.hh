#ifndef TRACK_BRANCHES_HH
#define TRACK_BRANCHES_HH

#include "TLorentzVector.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"

namespace xAOD {
  class IParticle;
  class Jet_v1;
  typedef Jet_v1 Jet;
}

// new addition - track systematics tools
#include "InDetTrackSystematicsTools/InDetTrackSmearingTool.h"
#include "InDetTrackSystematicsTools/InDetTrackBiasingTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthFilterTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginTool.h"
#include "InDetTrackSystematicsTools/JetTrackFilterTool.h"
#include "CxxUtils/make_unique.h"
using CxxUtils::make_unique;

//new addition - tracking CP tools
class IJetSelector;
class IJetCalibrationTool;
namespace InDet {
  class InDetTrackSelectionTool;
  class InDetTrackSmearingTool; //
  class InDetTrackBiasingTool; //
  class InDetTrackTruthFilterTool; //
  class InDetTrackTruthOriginTool; //
  class InDetTrackSystematicsTool; //
}
namespace CP {
  class CorrectionCode;
  class ITrackVertexAssociationTool;
  class IPileupReweightingTool; //
  class SystematicVariation; //
  class SystematicSet; //
}

class TTree;

#include <vector>
#include <string>

// branch buffers are stored as an external class to cut down on
// (re)compile time
struct TrackBranchBuffer;
struct BTagTrackAccessors;

enum TAGGERALGO{ IP2D=0,
     IP3D,
     SV0,
     SV1,
     JF };

class TrackBranches
{
public:
  typedef std::vector<const xAOD::IParticle*> PartVector;

  TrackBranches();
  ~TrackBranches();

  // disable copying and assignment
  TrackBranches& operator=(TrackBranches) = delete;
  TrackBranches(const TrackBranches&) = delete;

  void set_tree(TTree& output_tree, const std::string& prefix) const;
  void fill(const PartVector& constituents, const xAOD::BTagging &btag, const xAOD::Jet& orig_jet, const xAOD::VertexContainer& Primary_vertices, const xAOD::Vertex* myVertex, InDet::InDetTrackSmearingTool* smearingTool, InDet::InDetTrackBiasingTool* biasingTool);
  void clear();

private:
  TrackBranchBuffer* m_branches;
  BTagTrackAccessors* m_acc;


  bool particleInCollection( const xAOD::TrackParticle *trkPart, std::vector< ElementLink< xAOD::TrackParticleContainer > > trkColl );
  const xAOD::TruthParticle* truthParticle(const xAOD::TrackParticle *trkPart);
  int parent_classify(const xAOD::TruthParticle *theParticle);

  // short-circuit the branch filling if no tree is set
  mutable bool m_active;
};

#endif // TRACK_BRANCHES_HH
