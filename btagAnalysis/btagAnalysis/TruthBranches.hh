#ifndef TRUTH_BRANCHES_HH
#define TRUTH_BRANCHES_HH

// #include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"

#include "xAODJet/JetContainer.h"
#include "xAODJet/Jet.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/Vertex.h"
#include "TH1.h"
#include "TEfficiency.h"
class TTree;

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
}

// branch buffers are stored as an external class to cut down on
// (re)compile time // VD I strongly disagree with it ;-)
struct TruthBranchBuffer;

class TruthBranches
{
public:
  // might want to add a prefix to the constructor for the tree branches
  TruthBranches();
  ~TruthBranches();

  // disable copying and assignment
  TruthBranches& operator=(TruthBranches) = delete;
  TruthBranches(const TruthBranches&) = delete;

  // void set_tree(TTree& output_tree) const;
  void set_tree(TTree& output_tree, const std::string& name) const;  
  void fill(std::vector<const xAOD::TruthParticle*>& truthParticles);

  void clear();

private:

  TruthBranchBuffer* m_branches;
  // void DumpChildInfo(const xAOD::TruthParticle* part);

  // std::vector<int> m_truth_pdgId;
  // std::vector<int> m_truth_isCharged;
  // std::vector<double> m_truth_px;
  // std::vector<double> m_truth_py;
  // std::vector<double> m_truth_pz;  
  // std::vector<double> m_truth_pvtx_x;
  // std::vector<double> m_truth_pvtx_y;
  // std::vector<double> m_truth_pvtx_z;

  // std::vector<double> m_truth_dvtx_x;
  // std::vector<double> m_truth_dvtx_y;
  // std::vector<double> m_truth_dvtx_z;  




};

#endif // TRUTH_BRANCHES_HH

