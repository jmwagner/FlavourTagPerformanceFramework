#ifndef TRUTH_BRANCH_BUFFER_HH
#define TRUTH_BRANCH_BUFFER_HH

#include <vector>

struct TruthBranchBuffer {

  // std::vector<std::vector<int> > *example_vector_vector_branch;
  // std::vector<float> *example_vector_branch;

  // std::vector<std::vector<int> > *truth_pdgId;

  std::vector<int> *truth_pdgId;    
  std::vector<int> *  truth_isCharged;
  std::vector<int> *  truth_status;
  std::vector<int> *  truth_barcode;
  std::vector<double> *  truth_px;
  std::vector<double> *  truth_py;
  std::vector<double> *  truth_pz;
  std::vector<double> *  truth_pvtx_x;
  std::vector<double> *  truth_pvtx_y;
  std::vector<double> *  truth_pvtx_z;
  std::vector<double> *  truth_dvtx_x;
  std::vector<double> *  truth_dvtx_y;
  std::vector<double> *  truth_dvtx_z;

  std::vector<std::vector<int>> *  truth_parent_pdgId;
  std::vector<std::vector<int>> *  truth_parent_barcode;  
  std::vector<std::vector<int>> *  truth_child_pdgId;
  std::vector<std::vector<int>> *  truth_child_barcode;  
};

#endif // TRUTH_BRANCH_BUFFER_HH

