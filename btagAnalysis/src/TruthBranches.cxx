#include "../btagAnalysis/TruthBranches.hh"
#include "../btagAnalysis/TruthBranchBuffer.hh"

#include "xAODJet/Jet.h"
#include "AthContainers/exceptions.h"
#include "TTree.h"


#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"


//!-----------------------------------------------------------------------------------------------------------------------------!//
TruthBranches::TruthBranches():
  m_branches(new TruthBranchBuffer)
{
  // instantiate all the vectors here ...
  m_branches->truth_pdgId = new std::vector<int>();
  m_branches->truth_isCharged = new std::vector<int>();
  m_branches->truth_status = new std::vector<int>();
  m_branches->truth_barcode = new std::vector<int>();    
  m_branches->truth_px = new std::vector<double>();
  m_branches->truth_py = new std::vector<double>();
  m_branches->truth_pz = new std::vector<double>();
  m_branches->truth_pvtx_x = new std::vector<double>();
  m_branches->truth_pvtx_y = new std::vector<double>();
  m_branches->truth_pvtx_z = new std::vector<double>();
  m_branches->truth_dvtx_x = new std::vector<double>();
  m_branches->truth_dvtx_y = new std::vector<double>();
  m_branches->truth_dvtx_z = new std::vector<double>();

  m_branches->truth_parent_pdgId = new std::vector<std::vector<int>>();
  m_branches->truth_parent_barcode = new std::vector<std::vector<int>>();
  m_branches->truth_child_pdgId = new std::vector<std::vector<int>>();
  m_branches->truth_child_barcode = new std::vector<std::vector<int>>();

}

//!-----------------------------------------------------------------------------------------------------------------------------!//
TruthBranches::~TruthBranches() {
  // delete all the vectors here ...

  delete m_branches->truth_pdgId;
  delete m_branches->truth_isCharged;
  delete m_branches->truth_status;
  delete m_branches->truth_barcode;  
  delete m_branches->truth_px;
  delete m_branches->truth_py;
  delete m_branches->truth_pz;
  delete m_branches->truth_pvtx_x;
  delete m_branches->truth_pvtx_y;
  delete m_branches->truth_pvtx_z;
  delete m_branches->truth_dvtx_x;
  delete m_branches->truth_dvtx_y;
  delete m_branches->truth_dvtx_z;

  delete m_branches->truth_parent_pdgId;
  delete m_branches->truth_parent_barcode;
  delete m_branches->truth_child_pdgId;
  delete m_branches->truth_child_barcode;
  
  delete m_branches;
}

void TruthBranches::set_tree(TTree& output_tree, const std::string& name) const {  

  output_tree.Branch( "truth_pdgId"       , &m_branches->truth_pdgId );
  output_tree.Branch( "truth_isCharged"   , &m_branches->truth_isCharged );
  output_tree.Branch( "truth_status"      , &m_branches->truth_status );
  output_tree.Branch( "truth_barcode"     , &m_branches->truth_barcode );    

  output_tree.Branch( "truth_px"          , &m_branches->truth_px );
  output_tree.Branch( "truth_py"          , &m_branches->truth_py );
  output_tree.Branch( "truth_pz"          , &m_branches->truth_pz );

  output_tree.Branch( "truth_pvtx_x"       , &m_branches->truth_pvtx_x );
  output_tree.Branch( "truth_pvtx_y"       , &m_branches->truth_pvtx_y );
  output_tree.Branch( "truth_pvtx_z"       , &m_branches->truth_pvtx_z );      

  output_tree.Branch( "truth_dvtx_x"       , &m_branches->truth_dvtx_x );
  output_tree.Branch( "truth_dvtx_y"       , &m_branches->truth_dvtx_y );
  output_tree.Branch( "truth_dvtx_z"       , &m_branches->truth_dvtx_z );

  output_tree.Branch( "truth_parent_pdgId" , &m_branches->truth_parent_pdgId );
  output_tree.Branch( "truth_parent_barcode" , &m_branches->truth_parent_barcode );
  output_tree.Branch( "truth_child_pdgId" , &m_branches->truth_child_pdgId );
  output_tree.Branch( "truth_child_barcode" , &m_branches->truth_child_barcode );

}

//!-----------------------------------------------------------------------------------------------------------------------------!//


void TruthBranches::fill( std::vector<const xAOD::TruthParticle*>& truthParticles ) {
  for(const auto*  truth : truthParticles){

    double pvtx_x = -999;
    double pvtx_y = -999;
    double pvtx_z = -999;  

    double dvtx_x = -999;
    double dvtx_y = -999;
    double dvtx_z = -999;  

    TString prodVertex_pos = Form(",%9.3f,%9.3f,%9.3f", -999.999, -999.999, -999.999);
    TString decayVertex_pos = Form(",%9.3f,%9.3f,%9.3f", -999.999, -999.999, -999.999);  
    const xAOD::TruthVertex *prodVtx= nullptr;
    const xAOD::TruthVertex *decayVtx= nullptr;
    if(truth->hasProdVtx()) prodVtx = truth->prodVtx();
    if(truth->hasDecayVtx()) decayVtx = truth->decayVtx();
    if(prodVtx){
      prodVertex_pos = Form(",%9.3f,%9.3f,%9.3f", prodVtx->x(),prodVtx->y(),prodVtx->z());
      pvtx_x = prodVtx->x();
      pvtx_y = prodVtx->y();
      pvtx_z = prodVtx->z();    
    }
    if(decayVtx){
      decayVertex_pos = Form(",%9.3f,%9.3f,%9.3f", decayVtx->x(),decayVtx->y(),decayVtx->z());
      dvtx_x = decayVtx->x();
      dvtx_y = decayVtx->y();
      dvtx_z = decayVtx->z();    
    }

    std::vector<int> child_pdgId;
    std::vector<int> child_barcode;    
    std::vector<int> parent_pdgId;
    std::vector<int> parent_barcode;
    
    std::vector<const xAOD::TruthParticle*> children;    
    if( decayVtx){
      int nChildren = decayVtx->nOutgoingParticles();
      for( int i =0;i< nChildren;i++){
	const xAOD::TruthParticle* child = decayVtx->outgoingParticle(i);
	if(child){
	  children.push_back(child);
	  child_pdgId.push_back(child->pdgId());
	  child_barcode.push_back(child->barcode());
	}
      }
    }

    std::vector<const xAOD::TruthParticle*> parents;
    if( prodVtx){
      int nParents = prodVtx->nIncomingParticles();
      for( int i =0;i< nParents;i++){
	const xAOD::TruthParticle* parent = prodVtx->incomingParticle(i);
	if(parent){
	  parents.push_back(parent);
	  parent_pdgId.push_back(parent->pdgId());
	  parent_barcode.push_back(parent->barcode());
	}
      }
    }

    // double dist = 1;
    // if(prodVtx and decayVtx){
    //   Amg::Vector3D pvtx( prodVtx->x(), prodVtx->y(), prodVtx->z());
    //   Amg::Vector3D dvtx( decayVtx->x(), decayVtx->y(), decayVtx->z());  
    //   dist =(pvtx-dvtx).norm();
    // }
    // TString output_label = "TRUTH DUMP: pdgId,charged,px,py,pz,pvtxx,pvtxy,pvtxz,dvtxx,dvtxy,dvtx_z,dist:";
    // TString output_dist = Form(",%8.3f",dist);
    // TString output_vertex = prodVertex_pos  + decayVertex_pos + output_dist;
    // TString output_momentum = Form(",%8.3f,%8.3f,%8.3f",truth->px()/1000.,truth->py()/1000.,truth->pz()/1000.);
    // TString output_truth = Form("%6d,%2d",truth->pdgId(),truth->isCharged());  
    // TString output = output_label + output_truth + " " +output_momentum + " " + output_vertex;


    m_branches->truth_pdgId->push_back( truth->pdgId() );
    m_branches->truth_status->push_back( truth->status() );
    m_branches->truth_barcode->push_back( truth->barcode() );  
    m_branches->truth_isCharged->push_back(truth->isCharged());
    m_branches->truth_px->push_back(truth->px());
    m_branches->truth_py->push_back(truth->py());
    m_branches->truth_pz->push_back(truth->pz());  
    m_branches->truth_pvtx_x->push_back(pvtx_x);
    m_branches->truth_pvtx_y->push_back(pvtx_y);
    m_branches->truth_pvtx_z->push_back(pvtx_z);
    m_branches->truth_dvtx_x->push_back(dvtx_x);
    m_branches->truth_dvtx_y->push_back(dvtx_y);
    m_branches->truth_dvtx_z->push_back(dvtx_z);
    
    m_branches->truth_parent_pdgId->push_back(parent_pdgId);
    m_branches->truth_parent_barcode->push_back(parent_barcode);
    m_branches->truth_child_pdgId->push_back(child_pdgId);
    m_branches->truth_child_barcode->push_back(child_barcode);

  }
}

  //!-----------------------------------------------------------------------------------------------------------------------------!//
void TruthBranches::clear() {
  m_branches->truth_pdgId->clear();  
  m_branches->truth_isCharged->clear();
  m_branches->truth_status->clear();
  m_branches->truth_barcode->clear();    
  m_branches->truth_px->clear();
  m_branches->truth_py->clear();
  m_branches->truth_pz->clear();
  m_branches->truth_pvtx_x->clear();
  m_branches->truth_pvtx_y->clear();
  m_branches->truth_pvtx_z->clear();
  m_branches->truth_dvtx_x->clear();
  m_branches->truth_dvtx_y->clear();
  m_branches->truth_dvtx_z->clear();

  m_branches->truth_parent_pdgId->clear();
  m_branches->truth_parent_barcode->clear();
  m_branches->truth_child_pdgId->clear();
  m_branches->truth_child_barcode->clear();


}

// void TruthBranches::DumpChildInfo(const xAOD::TruthParticle* part) {

//   if(!part) return;

//   std::vector<const xAOD::TruthParticle*> children;
//   const xAOD::TruthVertex *prodVtx=0;
//   const xAOD::TruthVertex *decayVtx=0;
//   if(part->hasProdVtx()) prodVtx = part->prodVtx();
//   if(part->hasDecayVtx()) decayVtx = part->decayVtx();
//   if(part->hasDecayVtx()){
//     int nChildren = decayVtx->nOutgoingParticles();
//     for( int i =0;i< nChildren;i++){
//       const xAOD::TruthParticle* child = decayVtx->outgoingParticle(i);
//       if(child) children.push_back(child);
//     }
//   }


//   for(const auto* child : children){
//     DumpChildInfo(child);
//   }
//   children.clear();
  
//   if(abs(part->pdgId())==5) return;
  
//   bool isBHadron = part->isBottomHadron();
//   bool isCHadron = part->isCharmHadron();
//   double pvtx_x = -999;
//   double pvtx_y = -999;
//   double pvtx_z = -999;  

//   double dvtx_x = -999;
//   double dvtx_y = -999;
//   double dvtx_z = -999;  

//   TString prodVertex_pos = Form(",%9.3f,%9.3f,%9.3f", -999.999, -999.999, -999.999);
//   TString decayVertex_pos = Form(",%9.3f,%9.3f,%9.3f", -999.999, -999.999, -999.999);  
//   if(prodVtx){
//     prodVertex_pos = Form(",%9.3f,%9.3f,%9.3f", prodVtx->x(),prodVtx->y(),prodVtx->z());
//     pvtx_x = prodVtx->x();
//     pvtx_y = prodVtx->y();
//     pvtx_z = prodVtx->z();    
//   }
//   if(decayVtx){
//     decayVertex_pos = Form(",%9.3f,%9.3f,%9.3f", decayVtx->x(),decayVtx->y(),decayVtx->z());
//     dvtx_x = decayVtx->x();
//     dvtx_y = decayVtx->y();
//     dvtx_z = decayVtx->z();    
//   }

//   double dist = 1;
//   if(prodVtx and decayVtx){
//     Amg::Vector3D pvtx( prodVtx->x(), prodVtx->y(), prodVtx->z());
//     Amg::Vector3D dvtx( decayVtx->x(), decayVtx->y(), decayVtx->z());  
//     dist =(pvtx-dvtx).norm();
//   }
//   TString output_label = "TRUTH DUMP: pdgId,charged,px,py,pz,pvtxx,pvtxy,pvtxz,dvtxx,dvtxy,dvtx_z,dist:";
//   TString output_dist = Form(",%8.3f",dist);
//   TString output_vertex = prodVertex_pos  + decayVertex_pos + output_dist;
//   TString output_momentum = Form(",%8.3f,%8.3f,%8.3f",part->px()/1000.,part->py()/1000.,part->pz()/1000.);
//   TString output_part = Form("%6d,%2d",part->pdgId(),part->isCharged());  
//   TString output = output_label + output_part + " " +output_momentum + " " + output_vertex;


//   m_branches->truth_pdgId->push_back( part->pdgId() );
//   m_branches->truth_status->push_back( part->status() );
//   m_branches->truth_barcode->push_back( part->barcode() );    
//   m_branches->truth_isCharged->push_back(part->isCharged());
//   m_branches->truth_px->push_back(part->px());
//   m_branches->truth_py->push_back(part->py());
//   m_branches->truth_pz->push_back(part->pz());  
//   m_branches->truth_pvtx_x->push_back(pvtx_x);
//   m_branches->truth_pvtx_y->push_back(pvtx_y);
//   m_branches->truth_pvtx_z->push_back(pvtx_z);
//   m_branches->truth_dvtx_x->push_back(dvtx_x);
//   m_branches->truth_dvtx_y->push_back(dvtx_y);
//   m_branches->truth_dvtx_z->push_back(dvtx_z);


//   // if(dist>0.001 or abs(part->pdgId())==5)   std::cout << output << std::endl;
//   // std::cout << output << std::endl;  


// }

